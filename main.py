import numpy as np
from transformations import Transformations
from operations import Operations
from object import Object

transform = Transformations()
operations = Operations()

p1 = np.array([0,0,0,1], dtype=float)
p2 = np.array([0,0,4,1], dtype=float)
p3 = np.array([9,0,0,1], dtype=float)
p4 = np.array([0,1,0,1], dtype=float)

o = Object([p1,p2,p3,p4])
print "O,\n", o

'''
    QUESTAO 1
'''
S = transform.buildScaleMatrix(np.array([0.78567,7.07106,1.76776], dtype=float))
print "S,\n", S
transform.transform(o,S)
print "O',\n", o

'''
    Verificando a questao 1
'''
# print "p2 - p3", operations.distance(o.points[1], o.points[2])
# print "p2 - p4", operations.distance(o.points[1], o.points[3])
# print "p3 - p4", operations.distance(o.points[2], o.points[3])

'''
    QUESTAO 2
'''
T3 = transform.buildTranslateMatrix(np.array([-o.points[2][0], 0, 0, 1],dtype=float))
print "T3,\n", T3
transform.transform(o,T3)
print "O'',\n", o

u = o.points[3] - o.points[2]
print "U: ", u
print "Norm: ", operations.norm(u)
operations.normalize(u)
print "u: ", u