import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Object:

    points = []

    def __init__(self, pts=[]):
        self.points = pts

    def addPoint(self, p):
        self.points.append(p)

    def show(self):
        fig = plt.figure()
        ax = Axes3D(fig)
        lim = 0;
        
        x = []
        y = []
        z = []
        for p in self.points:
            x.append(p[0])
            y.append(p[1])
            z.append(p[2])

        x.append(self.points[0][0])
        y.append(self.points[0][1])
        z.append(self.points[0][2])
            
        x.append(self.points[2][0])
        y.append(self.points[2][1])
        z.append(self.points[2][2])
        
        ax.plot(x,z,y,'-o')
        ax.set_xlabel("x")
        ax.set_ylabel("z")
        ax.set_zlabel("y")
        
        for p in self.points:
            for pos in p:
                if pos > lim:
                    lim = pos
        
        lim += 2
        ax.set_xlim3d(0,lim)
        ax.set_ylim3d(0,lim)
        ax.set_zlim3d(0,lim)
        
        ax.invert_yaxis()
        plt.show()
        
    def __str__(self):
        res = "{\n"
        for p in self.points:
            res += "\t" + str(p) + "\n"
        res += "}"

        return res