import math as math

class Operations:
    def distance(self, a, b):
        sum = 0
        for i in range(0, 3):
            # print i
            sum += math.pow((a[i] - b[i]), 2)
        return math.sqrt(sum)

    def norm(self, v):
        sum = 0
        for i in v:
            sum += math.pow(i, 2)
        return math.sqrt(sum)

    def normalize(self, v):
        norm = self.norm(v)
        for i in range(3):
            v[i] = v[i]/norm
            
    def vectorialProduct(self, a, b):
        c = []
        c.append(a[1]*b[2] - a[2]*b[1])
        c.append(a[2]*b[0] - a[0]*b[2])
        c.append(a[0]*b[1] - a[1]*b[0])
        c.append(0)
        
        return c