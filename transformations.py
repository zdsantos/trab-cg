import numpy as np
import math as math
from object import Object

class Transformations:
    
    AXIS_X = "x"
    AXIS_Y = "y"
    AXIS_Z = "z"
    SURFACE_XY = "xy"
    SURFACE_YZ = "yz"
    SURFACE_XZ = "xz"
    DIRECTION_YX = "yx"
    DIRECTION_YZ = "yz"
    DIRECTION_XY = "xy"
    DIRECTION_XZ = "xz"
    DIRECTION_ZX = "zx"
    DIRECTION_ZY = "zy"

    def buildTranslateMatrix(self, t):
        '''
        ## Parametros
        t: vetor de transalacao
        '''
        m = np.eye(4, dtype=float)
        
        m[:,3] = t

        return m

    def buildScaleMatrix(self, s):
        '''
        ## Parametros
        s: vetor de escala em cada eixo
        '''
        m = np.eye(4, dtype=float)

        m[0,0] = s[0]
        m[1,1] = s[1]
        m[2,2] = s[2]
        
        return m

    def buildRotateMatrix(self, angle, axis):
        '''
        ## Parametros
        angle: angulo em graus
        axis: eixo de rotacao (variavel AXIS_)
        '''
        m = np.eye(4, dtype=float)
        rad = (angle * math.pi) / 180
        # print angle, " graus em rad: ", rad

        if(axis == Transformations.AXIS_X):
            m[1,1] = math.cos(rad)
            m[2,2] = math.cos(rad)
            m[1,2] = -math.sin(rad)
            m[2,1] = math.sin(rad)

        elif(axis == Transformations.AXIS_Y):
            m[0,0] = math.cos(rad)
            m[2,2] = math.cos(rad)
            m[0,2] = math.sin(rad)
            m[2,0] = -math.sin(rad)

        elif(axis == Transformations.AXIS_Z):
            m[0,0] = math.cos(rad)
            m[1,1] = math.cos(rad)
            m[0,1] = -math.sin(rad)
            m[1,0] = math.sin(rad)

        return m

    def buildMirrorMatrix(self, surface):
        '''
        ## Parametros
        surface: superficie de espelhamento (variavel SURFACE_)
        '''
        m = np.eye(4, dtype=float)

        if(surface == Transformations.SURFACE_XY):
            m[2,2] = -1
        elif(surface == Transformations.SURFACE_YZ):
            m[0,0] = -1
        elif(surface == Transformations.SURFACE_XZ):
            m[1,1] = -1

        return m

    def buildShearMatrix(self, angle, direction):
        '''
        ## Parametros
        angle: angulo em graus\n
        direction: [eixo perpendicular, direcao do movimento] (variavel DIRECTION_)
        '''
        m = np.eye(4, dtype=float)
        rad = (angle * math.pi) / 180
        # print angle, " graus em rad: ", rad

        if(direction == Transformations.DIRECTION_YX):
            m[0,1] = math.tan(rad)
        elif(direction == Transformations.DIRECTION_YZ):
            m[2,1] = math.tan(rad)
        elif(direction == Transformations.DIRECTION_XY):
            m[1,0] = math.tan(rad)
        elif(direction == Transformations.DIRECTION_XZ):
            m[2,0] = math.tan(rad)
        elif(direction == Transformations.DIRECTION_ZX):
            m[0,2] = math.tan(rad)
        elif(direction == Transformations.DIRECTION_ZY):
            m[1,2] = math.tan(rad)

        return m

    def buildMirrorAbrMatrix(self, normal):
        '''
        ## Parametros
        normal: vetor unitario normal ao espelho
        '''
        m = np.eye(4, dtype=float)
        n = np.array([normal])
        e = 2 * (normal * n.T)

        # print "normal:\n", normal
        # print "n:\n", n.T
        # print "e:\n", e

        for i in range(0,3):
            for j in range(0,3):
                m[i,j] = m[i,j] - e[i,j]

        return m

    def transform(self, obj, matrix):
        i = 0
        for p in obj.points:
            pl = np.zeros(4, dtype=float)
            np.dot(matrix, p, pl)
            obj.points[i] = pl
            i += 1